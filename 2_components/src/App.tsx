import React from "react";
import "./App.css";

function App() {
  return (
    <div className="layout" id="content">
      <div className="wrapper">
        <div className="content-page">
          <section className="extension">
            <div className="page">
              <h3>BRAMKARZE</h3>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/189/189-zoom.jpg"
                  className="fancy"
                  title="Bartłomiej  Drągowski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/189/189-mini-1.jpg"
                    alt="Bartłomiej  Drągowski"
                  />{" "}
                </a>
                <h6>Bartłomiej Drągowski</h6>
                <p>Data urodzenia: 19.08.1997</p>
                <p>Klub: Spezia Calcio</p>
                <p>Waga/wzrost: 74/191</p>
                <p>Mecze/gole: 2/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/190/190-zoom.jpg"
                  className="fancy"
                  title="Łukasz  Skorupski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/190/190-mini-1.jpg"
                    alt="Łukasz  Skorupski"
                  />{" "}
                </a>
                <h6>Łukasz Skorupski</h6>
                <p>Data urodzenia: 05.05.1991</p>
                <p>Klub: Bologna FC</p>
                <p>Waga/wzrost: 84/187</p>
                <p>Mecze/gole: 8/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/191/191-zoom.jpg"
                  className="fancy"
                  title="Wojciech  Szczęsny"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/191/191-mini-1.jpg"
                    alt="Wojciech  Szczęsny"
                  />{" "}
                </a>
                <h6>Wojciech Szczęsny</h6>
                <p>Data urodzenia: 18.04.1990</p>
                <p>Klub: Juventus FC</p>
                <p>Waga/wzrost: 85/196</p>
                <p>Mecze/gole: 73/0</p>
              </div>

              <h3>OBROŃCY</h3>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/192/192-zoom.jpg"
                  className="fancy"
                  title="Jan  Bednarek"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/192/192-mini-1.jpg"
                    alt="Jan  Bednarek"
                  />{" "}
                </a>
                <h6>Jan Bednarek</h6>
                <p>Data urodzenia: 12.04.1996</p>
                <p>Klub: Southampton FC</p>
                <p>Waga/wzrost: 77/189</p>
                <p>Mecze/gole: 49/1</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/193/193-zoom.jpg"
                  className="fancy"
                  title="Bartosz Bereszyński"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/193/193-mini-1.jpg"
                    alt="Bartosz Bereszyński"
                  />{" "}
                </a>
                <h6>Bartosz Bereszyński</h6>
                <p>Data urodzenia: 12.07.1992</p>
                <p>Klub: SSC Napoli</p>
                <p>Waga/wzrost: 77/183</p>
                <p>Mecze/gole: 51/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/197/197-zoom.jpg"
                  className="fancy"
                  title="Tomasz  Kędziora"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/197/197-mini-1.jpg"
                    alt="Tomasz  Kędziora"
                  />{" "}
                </a>
                <h6>Tomasz Kędziora</h6>
                <p>Data urodzenia: 11.06.1994</p>
                <p>Klub: PAOK Saloniki</p>
                <p>Waga/wzrost: 73/183</p>
                <p>Mecze/gole: 27/1</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/198/198-zoom.jpg"
                  className="fancy"
                  title="Jakub  Kiwior"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/198/198-mini-1.jpg"
                    alt="Jakub  Kiwior"
                  />{" "}
                </a>
                <h6>Jakub Kiwior</h6>
                <p>Data urodzenia: 15.02.2000</p>
                <p>Klub: Arsenal FC</p>
                <p>Waga/wzrost: 80/187</p>
                <p>Mecze/gole: 12/1</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/199/199-zoom.jpg"
                  className="fancy"
                  title="Arkadiusz Reca"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/199/199-mini-1.jpg"
                    alt="Arkadiusz Reca"
                  />{" "}
                </a>
                <h6>Arkadiusz Reca</h6>
                <p>Data urodzenia: 17.06.1995</p>
                <p>Klub: Spezia Calcio </p>
                <p>Waga/wzrost: 81/187</p>
                <p>Mecze/gole: 15/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/200/200-zoom.jpg"
                  className="fancy"
                  title="Mateusz Wieteska"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/200/200-mini-1.jpg"
                    alt="Mateusz Wieteska"
                  />{" "}
                </a>
                <h6>Mateusz Wieteska</h6>
                <p>Data urodzenia: 11.02.1997</p>
                <p>Klub: Clermont Foot 63</p>
                <p>Waga/wzrost: 77/187</p>
                <p>Mecze/gole: 2/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/253/253-zoom.jpg"
                  className="fancy"
                  title="Przemysław  Wiśniewski "
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/253/253-mini-1.jpg"
                    alt="Przemysław  Wiśniewski "
                  />{" "}
                </a>
                <h6>Przemysław Wiśniewski </h6>
                <p>Data urodzenia: 27.07.1998</p>
                <p>Klub: Spezia Calcio</p>
                <p>Waga/wzrost: 88/195</p>
                <p>Mecze/gole: 0/0</p>
              </div>

              <h3>POMOCNICY</h3>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/218/218-zoom.jpg"
                  className="fancy"
                  title="Krystian  Bielik"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/218/218-mini-1.jpg"
                    alt="Krystian  Bielik"
                  />{" "}
                </a>
                <h6>Krystian Bielik</h6>
                <p>Data urodzenia: 04.01.1998</p>
                <p>Klub: Derby County </p>
                <p>Waga/wzrost: 88/189</p>
                <p>Mecze/gole: 11/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/201/201-zoom.jpg"
                  className="fancy"
                  title="Przemysław Frankowski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/201/201-mini-1.jpg"
                    alt="Przemysław Frankowski"
                  />{" "}
                </a>
                <h6>Przemysław Frankowski</h6>
                <p>Data urodzenia: 12.04.1995</p>
                <p>Klub: RC Lens</p>
                <p>Waga/wzrost: 70/176</p>
                <p>Mecze/gole: 33/1</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/203/203-zoom.jpg"
                  className="fancy"
                  title="Jakub  Kamiński"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/203/203-mini-1.jpg"
                    alt="Jakub  Kamiński"
                  />{" "}
                </a>
                <h6>Jakub Kamiński</h6>
                <p>Data urodzenia: 05.06.2002</p>
                <p>Klub: VfL Wolfsburg </p>
                <p>Waga/wzrost: 71/179</p>
                <p>Mecze/gole: 10/1</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/251/251-zoom.jpg"
                  className="fancy"
                  title="Ben  Lederman"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/251/251-mini-1.jpg"
                    alt="Ben  Lederman"
                  />{" "}
                </a>
                <h6>Ben Lederman</h6>
                <p>Data urodzenia: 08.05.2000</p>
                <p>Klub: Raków Częstochowa</p>
                <p>Waga/wzrost: 75/182</p>
                <p>Mecze/gole: 0/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/206/206-zoom.jpg"
                  className="fancy"
                  title="Karol  Linetty"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/206/206-mini-1.jpg"
                    alt="Karol  Linetty"
                  />{" "}
                </a>
                <h6>Karol Linetty</h6>
                <p>Data urodzenia: 02.02.1995</p>
                <p>Klub: Torino FC</p>
                <p>Waga/wzrost: 73/176</p>
                <p>Mecze/gole: 44/5</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/188/188-zoom.jpg"
                  className="fancy"
                  title="Michał Skóraś"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/188/188-mini-1.jpg"
                    alt="Michał Skóraś"
                  />{" "}
                </a>
                <h6>Michał Skóraś</h6>
                <p>Data urodzenia: 15.02.2000</p>
                <p>Klub: Lech Poznań</p>
                <p>Waga/wzrost: 64/172</p>
                <p>Mecze/gole: 5/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/254/254-zoom.jpg"
                  className="fancy"
                  title="Bartosz Slisz "
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/254/254-mini-1.jpg"
                    alt="Bartosz Slisz "
                  />{" "}
                </a>
                <h6>Bartosz Slisz </h6>
                <p>Data urodzenia: 29.05.1999</p>
                <p>Klub: Legia Warszawa</p>
                <p>Waga/wzrost: 70/179</p>
                <p>Mecze/gole: 2/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/219/219-zoom.jpg"
                  className="fancy"
                  title="Damian Szymański"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/219/219-mini-1.jpg"
                    alt="Damian Szymański"
                  />{" "}
                </a>
                <h6>Damian Szymański</h6>
                <p>Data urodzenia: 16.06.1995</p>
                <p>Klub: AEK FC</p>
                <p>Waga/wzrost: 70/181</p>
                <p>Mecze/gole: 13/2</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/207/207-zoom.jpg"
                  className="fancy"
                  title="Sebastian Szymański"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/207/207-mini-1.jpg"
                    alt="Sebastian Szymański"
                  />{" "}
                </a>
                <h6>Sebastian Szymański</h6>
                <p>Data urodzenia: 10.05.1999</p>
                <p>Klub: Feyenoord Rotterdam</p>
                <p>Waga/wzrost: 58/174</p>
                <p>Mecze/gole: 23/1</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/208/208-zoom.jpg"
                  className="fancy"
                  title="Nicola  Zalewski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/208/208-mini-1.jpg"
                    alt="Nicola  Zalewski"
                  />{" "}
                </a>
                <h6>Nicola Zalewski</h6>
                <p>Data urodzenia: 23.01.2002</p>
                <p>Klub: AS Roma </p>
                <p>Waga/wzrost: 78/175</p>
                <p>Mecze/gole: 11/0</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/209/209-zoom.jpg"
                  className="fancy"
                  title="Piotr  Zieliński"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/209/209-mini-1.jpg"
                    alt="Piotr  Zieliński"
                  />{" "}
                </a>
                <h6>Piotr Zieliński</h6>
                <p>Data urodzenia: 20.05.1994</p>
                <p>Klub: SSC Napoli</p>
                <p>Waga/wzrost: 75/180</p>
                <p>Mecze/gole: 81/10</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/186/186-zoom.jpg"
                  className="fancy"
                  title="Mateusz  Łęgowski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/186/186-mini-1.jpg"
                    alt="Mateusz  Łęgowski"
                  />{" "}
                </a>
                <h6>Mateusz Łęgowski</h6>
                <p>Data urodzenia: 29.01.2003</p>
                <p>Klub: Pogoń Szczecin</p>
                <p>Waga/wzrost: 81/181</p>
                <p>Mecze/gole: 1/0</p>
              </div>

              <h3>NAPASTNICY</h3>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/211/211-zoom.jpg"
                  className="fancy"
                  title="Robert  Lewandowski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/211/211-mini-1.jpg"
                    alt="Robert  Lewandowski"
                  />{" "}
                </a>
                <h6>Robert Lewandowski</h6>
                <p>Data urodzenia: 21.08.1988</p>
                <p>Klub: FC Barcelona</p>
                <p>Waga/wzrost: 79/185</p>
                <p>Mecze/gole: 141/78</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/212/212-zoom.jpg"
                  className="fancy"
                  title="Arkadiusz  Milik"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/212/212-mini-1.jpg"
                    alt="Arkadiusz  Milik"
                  />{" "}
                </a>
                <h6>Arkadiusz Milik</h6>
                <p>Data urodzenia: 28.02.1994</p>
                <p>Klub: Juventus FC</p>
                <p>Waga/wzrost: 78/186</p>
                <p>Mecze/gole: 68/16</p>
              </div>

              <div className="kadra-box">
                <a
                  href="https://pzpn.pl/public/system/images/reprezentacja_players/214/214-zoom.jpg"
                  className="fancy"
                  title="Karol Świderski"
                >
                  <img
                    src="https://pzpn.pl/public/system/images/reprezentacja_players/214/214-mini-1.jpg"
                    alt="Karol Świderski"
                  />{" "}
                </a>
                <h6>Karol Świderski</h6>
                <p>Data urodzenia: 23.01.1997</p>
                <p>Klub: Charlotte FC</p>
                <p>Waga/wzrost: 77/184</p>
                <p>Mecze/gole: 21/9</p>
              </div>
              <div style={{clear: "both"}}></div>
            </div>
          </section>
        </div>
      </div>
    </div>
  );
}

export default App;
