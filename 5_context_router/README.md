# Portal nieruchomości

1. W pliku feed.json znajduje się baza danych portalu z nieruchomościami (uruchom ją jako rest api przy pomocy json-server)
2. Wczytaj wszykie nieruchomości na stronę główną - użyj useSwr lub React Query 
3. Stwórz podstrony dla wszystkich poszczególnych nieruchomości
4. Stwórz kontekts który pozwoli przetrzymać informacje o wyborach użytkowników
5. Stwórz formularz do wysyłania "lead'a"
6. Spróbuj wczytać komponenty w trybie Lazy
7. Zoptymalizuj context przy pomocy react-tracked
8. Stwórz filtrowanie produktów po kategoriach na stronie głównej. Zoptymalizuj filtrowanie przy pomocy useTransition.