# Authentication

1. Pod adresem http://auth-starter.noinputsignal.com/api/ znajdziesz api z bazą danych żartów
2. System pozwala zarejestrować nowego użytkownika, który będzie mógł po zalogowaniu dodać swój żart
3. Stwórz formularze dla rejestracji i logowania
4. Zaloguj użytkownika po czym przygotuj dla niego formularz do dodawania żarów